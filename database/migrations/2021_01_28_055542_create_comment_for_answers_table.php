<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentForAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_for_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('contents')->nullable(true);
            $table->timestamps();
            $table->unsignedBigInteger('answer_id');
            $table->unsignedBigInteger('profile_id');
            
            $table->foreign('answer_id')->references('id')->on('answers')->onDelete('cascade');
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comment_for_answers');
    }
}
